mkdir git_sample_project; cd git_sample_project
git init
touch a.txt b.txt c.txt
echo This is content of a.txt file. > a.txt
echo This is content of b.txt file. > b.txt
echo This is content of c.txt file. > c.txt
git add a.txt b.txt
git status
git commit -m 'Add a.txt and b.txt'
git status
git log
git add c.txt
git commit -m 'c.txt added'
git remote add origin https://gitlab.com/Hemant97Kumar/git_sample_project.git
git push origin master
cd ..
mkdir git_sample_project_2
git clone https://gitlab.com/Hemant97Kumar/git_sample_project.git git_sample_project_2/
cd git_sample_project_2/
touch d.txt
git add d.txt
git commit -m 'd.txt added'
git push origin master
cd ../git_sample_project
git pull origin master
cp ../git_drill_part_1.txt ./
git add git_drill_part_1.txt
git commit -m 'git_drill_part_1.txt added'
git push origin master
